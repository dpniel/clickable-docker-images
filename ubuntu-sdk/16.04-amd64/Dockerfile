FROM ubuntu:xenial
MAINTAINER Brian Douglass

# Disable problematic lzma compression - https://blog.packagecloud.io/eng/2016/03/21/apt-hash-sum-mismatch/
RUN echo 'Acquire::CompressionTypes::Order:: "gz";' > /etc/apt/apt.conf.d/99compression-workaround && \
    echo set debconf/frontend Noninteractive | debconf-communicate && \
    echo set debconf/priority critical | debconf-communicate

RUN echo "deb [arch=amd64] http://archive.ubuntu.com/ubuntu xenial main resticted multiverse universe" > /etc/apt/sources.list && \
    echo "deb [arch=amd64] http://archive.ubuntu.com/ubuntu xenial-updates main resticted multiverse universe" >> /etc/apt/sources.list && \
    echo "deb [arch=amd64] http://archive.ubuntu.com/ubuntu xenial-security main resticted multiverse universe" >> /etc/apt/sources.list && \
    apt-get update

RUN apt-get -y -f --no-install-recommends install gnupg ubuntu-keyring software-properties-common wget && \
    echo "deb http://repo.ubports.com xenial main" >> /etc/apt/sources.list && \
    wget -qO - http://repo.ubports.com/keyring.gpg | apt-key add - && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get update

RUN apt-get -y --no-install-recommends dist-upgrade && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get update && \
    apt-get -y --no-install-recommends install \
        apt-utils \
        build-essential \
        cmake \
        dpkg-cross \
        fakeroot \
        libc-dev \
        isc-dhcp-client \
        net-tools \
        ifupdown \
        g++-arm-linux-gnueabihf \
        pkg-config-arm-linux-gnueabihf \
        ubuntu-sdk-libs \
        ubuntu-sdk-libs-dev \
        ubuntu-sdk-libs-tools \
        oxideqt-codecs-extra \
        language-pack-en \
        click \
        qtquickcontrols2-5-dev \
        qtbase5-private-dev \
        qtdeclarative5-private-dev \
        libqt5opengl5-dev \
        mercurial \
        git \
        libicu-dev \
        qtfeedback5-dev \
        qtsystems5-dev \
        qml-module-io-thp-pyotherside \
        morph-webapp-container \
        libconnectivity-qt1-dev \
        && \
    apt-get clean

# Install cordova
RUN wget -qO- https://deb.nodesource.com/setup_10.x | bash - && \
    apt-get install -y nodejs && \
    npm install -g cordova@7.0.0

# Install go
RUN wget https://storage.googleapis.com/golang/go1.6.linux-amd64.tar.gz && \
    tar -xvf go1.6.linux-amd64.tar.gz && \
    mv go /usr/local && \
    ln -s /usr/include/x86_64-linux-gnu/qt5/QtCore/5.9.5/QtCore /usr/include/ && \
    rm go1.6.linux-amd64.tar.gz

# TODO this probably needs to be fixed upstream
ADD ubuntu-click-tools.prf /usr/lib/x86_64-linux-gnu/qt5/mkspecs/features/ubuntu-click-tools.prf

ENV CGO_ENABLED=1
